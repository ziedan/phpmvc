<?php
class App{
    protected $controller = 'Home';      //properti default controller adalah home
    protected $method = 'index';
    protected $params = [];             //parameter berupa array

    public function __construct(){
        $url = $this->parseURL();
        
        //membaca url untuk mementukan controllers mana yang dijalankan
        if(file_exists('../app/controllers/'. $url[0] .'.php')){   // cek file contoller
            $this->controller = $url[0];    //isi properti controller dari url (array) index ke 0
            unset($url[0]); // menghilangkan array index ke 0 dari variable url
        }
        require_once '../app/controllers/'. $this->controller . '.php'; // panggil file controller sesuai url
        $this->controller = new $this->controller;  // buat object baru

        //membaca url untuk mementukan method
        if(isset($url[1])){ //cek url index ke 1
            if(method_exists($this->controller, $url[1])){   //cek method
                $this->method=$url[1];
                unset($url[1]);
            }
        }

        //parameter
        if(!empty($url)){
            $this->params= array_values($url);
        }

       call_user_func_array([$this->controller,$this->method], $this->params);
    }

    public function parseURL(){
        if(isset($_GET['url'])){
            $url = rtrim($_GET['url'],'/');  // menghilangkan tanda / di akhir url
            $url = filter_var($url, FILTER_SANITIZE_URL);  // menghindari input inject pada url
            $url = explode('/',$url);   // memecah url berdasarkan /
            return $url;
        }
    }
}