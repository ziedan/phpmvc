<?php
class Controller{
    //function untuk memanggil views
    public function view($view, $data=[]){
        require_once '../app/views/'. $view .'.php';
    }

    // function untuk memanggil models
    public function model($model){
        require_once '../app/models/'. $model .'.php';
        return new $model;
    }
}