<?php
class Mahasiswa_model{
    private $table = 'mahasiswa';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllMahasiswa(){
        $this->db->query('SELECT * FROM '. $this->table);
        return $this->db->resultSet();
    }

    public function getMahasiswaById($id){
        $this->db->query('SELECT * FROM '. $this->table .' WHERE id=:id');
        $this->db->bind('id',$id);
        return $this->db->single();
    }

    public function addDataMahasiswa($dataMahasiswa){
        $qry = "INSERT INTO mahasiswa VALUES ('',:nama,:nrp,:email,:jurusan)";
        $this->db->query($qry);
        $this->db->bind('nama',$dataMahasiswa['nama']);
        $this->db->bind('nrp',$dataMahasiswa['nrp']);
        $this->db->bind('email',$dataMahasiswa['email']);
        $this->db->bind('jurusan',$dataMahasiswa['jurusan']);

        $this->db->execute();
        return $this->db->hitungBaris();
    }

    public function delDataMahasiswa($id){
        $qry = "DELETE FROM mahasiswa WHERE id = :id";
        $this->db->query($qry);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->hitungBaris();
    }

    public function ubahDataMahasiswa($dataMahasiswa){
        $qry = "UPDATE mahasiswa SET 
                    nama= :nama,
                    nrp= :nrp,
                    email= :email,
                    jurusan= :jurusan
                    WHERE id=:id";
        $this->db->query($qry);
        $this->db->bind('nama',$dataMahasiswa['nama']);
        $this->db->bind('nrp',$dataMahasiswa['nrp']);
        $this->db->bind('email',$dataMahasiswa['email']);
        $this->db->bind('jurusan',$dataMahasiswa['jurusan']);
        $this->db->bind('id',$dataMahasiswa['id']);

        $this->db->execute();
        return $this->db->hitungBaris();
    }

    public function cariDataMahasiswa(){
        $keyword = $_POST['keyword'];
        $qry = "SELECT * FROM mahasiswa WHERE nama LIKE :keyword";
        $this->db->query($qry);
        $this->db->bind('keyword','%'.$keyword.'%');
        return $this->db->resultSet();
    }
}